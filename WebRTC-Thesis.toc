\contentsline {chapter}{\numberline {1}Introduction}{4}{chapter.1}
\contentsline {section}{\numberline {1.1}From Real Time Communication to WebRTC}{5}{section.1.1}
\contentsline {section}{\numberline {1.2}WebRTC Architecture}{6}{section.1.2}
\contentsline {section}{\numberline {1.3}Behaviour of WebRTC in Browsers}{8}{section.1.3}
\contentsline {section}{\numberline {1.4}Purpose of this work}{10}{section.1.4}
\contentsline {chapter}{\numberline {2}WebRTC Signaling}{11}{chapter.2}
\contentsline {section}{\numberline {2.1}JSEP: JavaScript Session Establishment Protocol}{14}{section.2.1}
\contentsline {section}{\numberline {2.2}WebRTC Media Connections: ICE, STUN, TURN}{16}{section.2.2}
\contentsline {section}{\numberline {2.3}SDP: Session Description Protocol}{19}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Media and Transport Information}{20}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Timing Information}{21}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Private Sessions}{21}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Specification}{21}{subsection.2.3.4}
\contentsline {subsection}{\numberline {2.3.5}Media Descriptors - "m="}{24}{subsection.2.3.5}
\contentsline {chapter}{\numberline {3}WebRTC streaming}{25}{chapter.3}
\contentsline {section}{\numberline {3.1}RTP - Real-Time Transport Protocol}{25}{section.3.1}
\contentsline {section}{\numberline {3.2}RTCP - Real-Time Transport Control Protocol}{27}{section.3.2}
\contentsline {chapter}{\numberline {4}Application}{30}{chapter.4}
\contentsline {section}{\numberline {4.1}WebRTC Architecture Across Browsers}{30}{section.4.1}
\contentsline {section}{\numberline {4.2}Mozilla Firefox WebRTC Modifications}{32}{section.4.2}
\contentsline {section}{\numberline {4.3}WebRTC Multicast Client}{34}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Server}{34}{subsection.4.3.1}
\contentsline {subsubsection}{\numberline {4.3.1.1}NodeJS}{35}{subsubsection.4.3.1.1}
\contentsline {subsubsection}{\numberline {4.3.1.2}MongoDB}{35}{subsubsection.4.3.1.2}
\contentsline {subsubsection}{\numberline {4.3.1.3}Reliable Signaler}{36}{subsubsection.4.3.1.3}
\contentsline {subsubsection}{\numberline {4.3.1.4}Server Implementation}{37}{subsubsection.4.3.1.4}
\contentsline {subsection}{\numberline {4.3.2}Client}{37}{subsection.4.3.2}
\contentsline {subsubsection}{\numberline {4.3.2.1}Angular 2}{37}{subsubsection.4.3.2.1}
\contentsline {subsubsection}{\numberline {4.3.2.2}TypeScript}{38}{subsubsection.4.3.2.2}
\contentsline {subsubsection}{\numberline {4.3.2.3}Angular Material 2}{39}{subsubsection.4.3.2.3}
\contentsline {subsubsection}{\numberline {4.3.2.4}Implementation}{39}{subsubsection.4.3.2.4}
\contentsline {subsubsection}{\numberline {4.3.2.5}Browser Support}{42}{subsubsection.4.3.2.5}
\contentsline {chapter}{\numberline {5}Conclusions and Future Work}{43}{chapter.5}
